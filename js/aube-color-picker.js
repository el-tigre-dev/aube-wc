'use strict';

acf.add_filter('color_picker_args', function (args, $field) {
    // do something to args
    args.palettes = ['#00966C', '#F8C1B8', '#FFDCD5', '#C0A571', '#ffffff'];

    // return
    return args;
});

//# sourceMappingURL=aube-color-picker.js.map