'use strict';

(function () {
	var container = document.getElementsByClassName('products')[0];
	var next = document.querySelector('.next');
	if (!next) {
		document.querySelector('.infinite-scroll__loader').style.display = 'none';
		return;
	}
	var infScroll = new InfiniteScroll(container, {
		path: '.next',
		append: '.product',
		history: false,
		hideNav: '.woocommerce-pagination',
		status: '.infinite-scroll__loader'
	});
})();

//# sourceMappingURL=shop-page.js.map