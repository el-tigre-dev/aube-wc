'use strict';

/**
 * AJAX
 */
function post(args, callback, isFormData) {
	var isFormData = isFormData || false;

	var request = new XMLHttpRequest();

	if (isFormData) {
		var params = new FormData(args['form']);

		if (args['action']) params.append('action', args['action']);
		if (args['nonce']) params.append('nonce', args['nonce']);
	} else {
		var params = '';

		for (var key in args) {
			params += key + '=' + args[key] + '&';
		}params = params.substring(0, params.length - 1);
	}

	request.onload = function () {
		if (request.status >= 200 && request.status < 400) {
			callback(JSON.parse(request.responseText));
		} else {
			callback({ success: false, data: { error: 'server' } });
		}
	};

	request.onerror = function () {
		callback({ success: false, data: { error: 'connection' } });
	};

	request.open('POST', aube.ajaxUrl, true);
	if (!isFormData) request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.send(params);
}

/**
 * Forms Front Validation
 */
var forms = document.querySelectorAll('form');

for (var i = forms.length - 1; i >= 0; i--) {
	forms[i].noValidate = true;
	forms[i].addEventListener('submit', function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopImmediatePropagation();
		}
	});

	var formElements = forms[i].querySelectorAll('input, textarea, select');
	for (var j = formElements.length - 1; j >= 0; j--) {
		formElements[j].insertAdjacentHTML('afterend', '<div class="aube__error"></div>');
		formElements[j].addEventListener('invalid', function (e) {
			var message = this.hasAttribute('data-message') ? this.getAttribute('data-message') : this.validationMessage;
			this.nextSibling.textContent = message;
		});

		formElements[j].addEventListener('blur', function (e) {
			this.nextSibling.textContent = '';
			this.checkValidity();
		});
	}
}
;(function(/* BrowserSync-Brunch */) {
  var url = "//" + location.hostname + ":3000/browser-sync/browser-sync-client.2.1.6.js";
  var bs = document.createElement("script");
  bs.type = "text/javascript"; bs.async = true; bs.src = url;
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(bs, s);
})();;
//# sourceMappingURL=aube.js.map