<?php 	
	wp_enqueue_script( 'swiper' );
	wp_enqueue_script( 'aube-new-products' );
?>

<section class="section-new-products container">

	<h2 style="color:<?php echo $section['text_color']; ?>" class="post__title"><?php echo $section['title']; ?></h2>

	<?php /* woocommerce_product_loop_start(); */ ?>
	<?php
		
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'tax_query' => array(
			    array(
			        'taxonomy' => 'product_visibility',
			        'field'    => 'name',
			        'terms'    => 'exclude-from-catalog',
			        'operator' => 'NOT IN',
			    ),
			),
			'limit'        => '12',
			'columns'      => '4',
			'orderby'      => 'date',
			'order'        => 'DESC',
			'category'     => '',
			'cat_operator' => 'IN',
		);
		$loop = new WP_Query( $args );
		
	?>
			
	<!-- Slider main container -->
	<div class="swiper-container"><!-- related-swiper -->
		
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper products">
		
			<?php if ( $loop->have_posts() ) :  while ( $loop->have_posts() ) : $loop->the_post(); /* global $product; */ ?>
				<?php
				/*
				 	$post_object = get_post( $loop->get_id() );
					setup_postdata( $GLOBALS['post'] =& $post_object );
				*/
				?>
				<div class="swiper-slide">
					<?php wc_get_template_part( 'content', 'product' ); ?>
				</div>
				
			<?php endwhile; endif; ?>
		
		</div>
		<!-- If we need navigation buttons -->
		<div class="swiper-button-prev-np"></div>
		<div class="swiper-button-next-np"></div>
		
    </div>
		    
		
	<?php /* woocommerce_product_loop_end(); */ ?>
		
</section>