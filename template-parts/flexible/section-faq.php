<?php wp_enqueue_script( 'aube-faq' ); ?>

<?php $index = 0; ?>
<section class="section-faq container">
	
	<?php if ($section['separator_above_my_head']) : ?>
		<hr class="separator-faq">
	<?php endif; ?>
	
	<h2 class="faq__title"><?php echo $section['faq_title']; ?></h2>
	
	<?php if ($section['questions-answers']) : ?>
		<ul class="faq">
			<?php foreach ( $section['questions-answers'] as $i => $question_answer ) : ?>
			
				<li class="question-answer faq-<?php echo $i; ?>">
					
					<h3 class="question__down-arrow" id="question<?php echo $i; ?>" onclick="toggleItem('<?php echo $i; ?>')"> <?php echo $question_answer['question']; ?></h3>
						
	                <div class="answer answer--hidden" style="display:none" id="paragraph<?php echo $i; ?>"><?php echo $question_answer['answer']; ?>
	                <hr class="separator-faq">
	                </div>                
	                
	            </li>            
				
			<?php endforeach; ?>		
	    </ul>
	<?php endif; ?>
    

    <div class="ligne__wrapper">
		<img class="ligne" src="<?php echo get_template_directory_uri(); ?>/assets/images/ligne.png">
	</div>	

        
</section>

