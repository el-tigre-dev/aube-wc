<?php
	wp_enqueue_script( 'swiper' );
	wp_enqueue_script( 'aube-banner-swiper' );
?>

<section class="slider">

    <div class="swiper-container banner-swiper">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <?php foreach( $section['slider'] as $slider ) : ?>
                <div class="swiper-slide">
                    <div class="swiper-slide__container"></div>
                    <div class="product_image_container">
                        <?php echo wp_get_attachment_image( $slider['image']['id'] , 'home_image' , false , array( 'class' => '' ) ); ?>
                    </div>
					
					<a href="<?php echo $slider['cta_link']; ?>">
	                    <div class="swiper__text-container <?php if ($slider['position'] == 'right') echo 'swiper__text-container--right'; ?>">
	                        <?php if ( !empty( $slider['title'] ) ) : ?>
	                            <div class="swiper__title" style="color:<?php echo $slider["title_color"]; ?>"><?php echo $slider["title"]; ?></div>
	                        <?php endif; ?>
	
	                        <?php if ( !empty( $slider['subtitle'] ) ) : ?>
	                            <div class="swiper__subtitle" style="color:<?php echo $slider["subtitle_color"]; ?>"><?php echo $slider["subtitle"]; ?></div>
	                        <?php endif; ?>
	
	                        <?php if ( !empty( $slider['section_text'] ) ) : ?>
	                            <div class="swiper__text"><?php echo $slider["section_text"]; ?></div>
	                        <?php endif; ?>   
	
	                        <?php if ( !empty( $slider['cta_link'] ) ) : ?>
	                            <a class="cta" href="<?php echo $slider['cta_link']; ?>"><img src="<?php echo $slider['button']['url']; ?>" alt="bon bouquet"/></a>
	                        <?php endif; ?>
	                    </div>
					</a>
                </div> 

            <?php endforeach; ?>
        </div>

   
        <!-- If we need pagination -->
        <!-- <div class="swiper-pagination"></div> -->
    
        <!-- If we need navigation buttons -->
        <!-- <div class="controls-button"> -->
            <div class="swiper-button-prev" ></div>
            <div class="swiper-button-next" ></div>
        <!-- </div> -->
    
        <!-- If we need scrollbar -->
        <!-- <div class="swiper-scrollbar"></div> -->
    </div>
</section>

