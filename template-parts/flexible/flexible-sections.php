<?php 
if ( !empty( $fields['section'] ) ) :
	foreach ( $fields['section'] as $section ) :
		
		switch ( $section['acf_fc_layout'] ) :

			case 'section_slider' :
				include( locate_template( 'template-parts/flexible/section-slider.php' ) ); break;	
			case 'section_faq' :
				include( locate_template( 'template-parts/flexible/section-faq.php' ) ); break;	
			case 'section_new_products' :
				include( locate_template( 'template-parts/flexible/section-new-products.php' ) ); break;

		endswitch;

	endforeach;
endif;
?>