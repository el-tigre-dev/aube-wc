<?php
if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'Aube_WC_Pop_In' ) ) :

class Aube_WC_Pop_In {

	public function __construct() {
		if ( is_admin() ) {
			include_once 'admin/class-pop-in-admin.php';
			new Aube_WC_Pop_In_Admin();
		}

		include_once 'public/class-pop-in-public.php';
		new Aube_WC_Pop_In_Public();
	}
}

new Aube_WC_Pop_In();

endif;