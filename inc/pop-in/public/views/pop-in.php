<div class="popin-aube">

    <div class="popin-overlay" id="popin-overlay"></div>

    <svg aria-hidden="true" width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64" id="popin-closer" class="popin__closer">
        <path fill="white" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59 c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59 c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
    </svg>

    <div class="popin-container">

        <div id="pop-in-newsletter" class="popin popin-vertical " aria-hidden="true">

            <div class="pop-in__background-color">

                <div class="pop-in__left-part">

                    <h2> <?php echo $pop_in["title"] ?> </h2>
                    <p> <?php echo $pop_in["subtitle"] ?> </p>

                    <?php if ( $pop_in["script_or_call_to_action"] == "cta" ) : ?>
                        <a  class="popin-cta" href="<?php echo $pop_in["cta_url"] ?>"><?php echo $pop_in["cta_label"] ?></a>
                    <?php elseif ( $pop_in["script_or_call_to_action"] == "script" ) : ?>
                        <?php echo $pop_in["script_form"] ?>
                    <?php endif; ?>

                </div>

                <div class="pop-in__right-part" style="background-image:url('<?php echo $pop_in["background"]["url"]  ?>') ">            
                    <div class="img-container">
                        <img src="<?php echo $pop_in["logo"]['url'] ?>">
                    </div>
                </div>

            </div>
        </div>
    </div>    

</div>