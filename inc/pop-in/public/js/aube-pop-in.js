'use strict';

(function () {

    var popinOverlay = document.getElementById('popin-overlay');
    var popin = document.getElementById('pop-in-newsletter');
    var popinCloser = document.getElementById('popin-closer');

    var cookie_duration = aubePopin['cookie_duration'];

    if (aubePopin["display_mode"] == 'scroll') {

        $(window).scroll(function (e) {
            if (getCookie("show-modal") != "no" || getCookie("show-modal") == false) {
                var scrollval = $(window).scrollTop();
                if (scrollval >= aubePopin["scroll"]) {
                    popinOverlay.classList.add('popin-overlay--visible');
                    popin.classList.add('popin--visible');
                    popin.setAttribute('aria-hidden', 'false');
                    popinCloser.setAttribute('aria-hidden', 'false');
                }
            }
        });
    }

    if (aubePopin["display_mode"] == 'time') {
        if (getCookie("show-modal") != "no" || getCookie("show-modal") == false) {
            setTimeout(function () {
                popinOverlay.classList.add('popin-overlay--visible');
                popin.classList.add('popin--visible');
                popin.setAttribute('aria-hidden', 'false');
                popinCloser.setAttribute('aria-hidden', 'false');
            }, aubePopin["time"] * 1000);
        }
    }

    if (aubePopin["display_mode"] == 'user_leaves') {

        addEvent(window, "load", function (e) {
            addEvent(document, "mouseleave", function (e) {
                if (getCookie("show-modal") == "yes" || !getCookie("show-modal")) {
                    e = e ? e : window.event;
                    var from = e.relatedTarget || e.toElement;
                    if (!from || from.nodeName == "HTML") {
                        popinOverlay.classList.add('popin-overlay--visible');
                        popin.classList.add('popin--visible');
                        popin.setAttribute('aria-hidden', 'false');
                        popinCloser.setAttribute('aria-hidden', 'false');

                        var submitButton = document.querySelector('.popin .actions input[type=submit]');
                        if (submitButton != null) {
                            submitButton.addEventListener('click', function (e) {
                                setTimeout(function () {
                                    var submittedMessages = document.querySelector('.popin .submitted-message');
                                    if (submittedMessages != null) {
                                        closePopin(e);
                                        //console.log(getCookie("show-modal"));
                                    }
                                }, 2000);
                            });
                        }
                    }
                }
            });
        });
    }
    function closePopin(e) {
        e.preventDefault();

        /* Luc 26/04/19 : J'ai voulu tout dégager plutôt que cacher
        document.getElementById('popin-overlay').classList.remove('popin-overlay--visible');
        popin.setAttribute('aria-hidden', 'true');
        popinCloser.setAttribute('aria-hidden', 'true');
        */
        var childToRemove = document.querySelector('.popin-aube');
        childToRemove.parentNode.removeChild(childToRemove);

        // document.body.classList.remove('no-scroll');
        var today = new Date();
        var expiration_date = new Date();
        expiration_date.setDate(today.getDate() + Number(cookie_duration));
        document.cookie = 'show-modal=no; expires=' + expiration_date + '; path=/';
    }

    popinCloser.addEventListener('click', closePopin);
    document.addEventListener('keyup', function (e) {
        if (e.keyCode === 27) closePopin(e);
    });

    function getCookie(name) {
        var pattern = RegExp(name + "=.[^;]*");
        var matched = document.cookie.match(pattern);
        if (matched) {
            var cookie = matched[0].split('=');
            return cookie[1];
        }
        return false;
    }
    function addEvent(obj, evt, fn) {
        if (obj.addEventListener) {
            obj.addEventListener(evt, fn, false);
        } else if (obj.attachEvent) {
            obj.attachEvent("on" + evt, fn);
        }
    }
})();

//# sourceMappingURL=aube-pop-in.js.map