<?php
if( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'Aube_WC_Pop_In_Public' ) ) :

class Aube_WC_Pop_In_Public {

	private $dir_path;

	public function __construct() {
		$this->dir_path = get_template_directory_uri() . '/inc/pop-in/public/';
        
        /*
        add_action( 'wp_ajax_show_modal', array( $this, 'show_modal' ) );
        add_action( 'wp_ajax_nopriv_show_modal', array( $this, 'show_modal' ) );
        */

        add_action( 'wp_enqueue_scripts', array( $this, 'pop_in_public' ) );

	}
    
    /*
    function show_modal(){

        $pop_in = get_field( 'pop-in', 'global-options' );

        $response = array( 
            "success" => true ,
            "display_mode" => $pop_in["display_mode"],
            "time" => $pop_in["time"],
            "scroll" => $pop_in["scroll"],
            "cookie_duration" => $pop_in["cookie_duration"]
            );
        wp_send_json( $response ) ;
    }
    */


    public function pop_in_public() {

        $current_page = get_the_ID();
        $pop_in_enabled = get_field( 'pop-in_enabled', 'global-options' );
        $excluded_pages = get_field( 'excluded_pages', 'global-options' );

        if ( !isset( $pop_in_enabled ) || !$pop_in_enabled || in_array( $current_page, $excluded_pages ) ) return;

        $pop_in = get_field( 'pop-in', 'global-options' );

		wp_enqueue_script( 'aube-pop-in', $this->dir_path . 'js/aube-pop-in.js', array( 'aube' ), Aube::$version, true );
        wp_localize_script( 'aube-pop-in', 'aubePopin', 
            array(
                "display_mode" => $pop_in["display_mode"],
                'time' => $pop_in["time"],	
                "scroll" => $pop_in["scroll"],
                "cookie_duration" => $pop_in["cookie_duration"],
                'nonce'   => wp_create_nonce( 'aube-pop-in' ),
				'action' => 'aube_pop_in'
            )
        );

        $image = $pop_in["background"];
		include_once 'views/pop-in.php';  

    }
    
}

endif;