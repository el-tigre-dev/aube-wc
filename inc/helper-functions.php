<?php
function aube_is_filled( $field ) {
	return isset( $field ) && !empty( $field );
}

function aube_current_url() {
	global $wp;

	return home_url( $wp->request ) . '/';
}

function aube_debug( $field ) {
	echo '<pre>';
	var_dump( $field );
	echo '</pre>';
}