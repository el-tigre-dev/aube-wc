<?php wp_enqueue_script( 'aube-banner', get_template_directory_uri() . '/inc/banner/public/js/aube-banner.js', array(), false, true ); ?>

<?php 
    if ( get_field( 'banner_enabled', 'global-options' ) && !wp_is_mobile() ) : 
        $options = get_fields( 'global-options' ); ?>
        <div id="banner" class="banner" aria-hidden="false" style="background-color: <?php echo $options['banner_background_color']; ?>">
            <?php 
            if ($options['banner_link_enabled'] != "" ) :
                if ( $options['banner_link_destination'] === 'external' )
                    $url = $options['banner_link_external'];
                else
                    $url = $options['banner_link_internal_destination'] === 'post' ? $options['banner_link_internal'] : get_permalink( $options['banner_link_internal_product'][0] );
            endif;
            ?>
            
            <div class="banner__text" style="color: <?php if (isset($options['banner_text_color'])) echo $options['banner_text_color']; else echo '#000' ?>">
                <?php if ( function_exists('pll_the_languages') ) : ?>
                    <ul class="languages-switcher"><?php pll_the_languages( array( 'display_names_as' => 'slug' ) ); ?></ul>
                <?php endif; ?>
                <?php if ( $options['banner_link_enabled'] ) : ?>
                <a class="banner__link" href="<?php echo $url; ?>">
                <?php endif; ?>

                <?php echo $options['banner_text']; ?>

                <?php if ( $options['banner_link_enabled'] ) : ?>
                    </a>
                <?php endif; ?>
            </div>

            <div class="banner-closer" id="banner-closer">X</div>

        </div>
    <?php endif; 
?>