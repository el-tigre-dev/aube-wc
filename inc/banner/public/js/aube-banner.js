(function () {
    'use strict';

    var banner = document.getElementById('banner');
    var bannerCloser = document.getElementById('banner-closer');
    var site = document.getElementById('page');

    if (!sessionStorage.getItem('banner')) {
        banner.setAttribute('aria-hidden', 'false');
        site.classList.add('site--mini-banner');
        // window.addEventListener('resize', responsive);
    }

    function closeBanner(e) {
        e.preventDefault();
        banner.setAttribute('aria-hidden', 'true');
        site.classList.remove('site--mini-banner');
        sessionStorage.banner = 'disabled';
    }

    function responsive() {
        banner.setAttribute('aria-hidden', window.innerWidth <= 768);
    }

    bannerCloser.addEventListener('click', closeBanner);
    // window.addEventListener('resize', responsive);
})();
