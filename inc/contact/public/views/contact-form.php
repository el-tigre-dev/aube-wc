<section id="contact-section" class="auve-contact__section">
	<p><?php _e( 'Forgotten password', 'aube' ); ?></p2>

	<form id="contact-form" novalidate>
		<select id="subjects-select" name="subject" class="problem-form__select" required>
			<option disabled selected><?php _e( 'Choose your subject', 'lou-yetu' );?></option>
			<?php if ( !empty( $subjects ) ) : ?>
				<?php foreach( $subjects as $subject ) : ?>
					<option value="<?php echo $subject['subject']; ?>"><?php echo $subject['subject']; ?></option>
				<?php endforeach; ?>
			<?php else: ?>
				<option><?php _e( 'No subject yet', 'aube' ); ?></option>
			<?php endif; ?>
		</select>

		<label class="aube__label" for="contact-firstname"><?php _e( 'Firstname', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" id="contact-firstname" type="text" name="first_name" placeholder="<?php _e( 'e.g. John', 'aube' ); ?>" required>

		<label class="aube__label" for="contact-lastname"><?php _e( 'Lastname', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" id="contact-lastname" type="text" name="last_name" placeholder="<?php _e( 'e.g. Doe', 'aube' ); ?>" required>

		<label class="aube__label" for="contact-email"><?php _e( 'Email', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" id="contact-email" type="email" name="email" placeholder="<?php _e( 'e.g. john@doe.com', 'aube' ); ?>" required>

		<label class="aube__label" for="contact-phone"><?php _e( 'Phone number', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" id="contact-phone" type="text" name="phone" required>

		<label class="aube__label" for="contact-order"><?php _e( 'Order number', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" id="contact-order" type="text" name="order" placeholder="<?php _e( 'e.g. 11305', 'aube' ); ?>" required>

		<label class="aube__label" for="contact-message"><?php _e( 'Message', 'aube' ); ?><span class="required">*</span></label>
		<textarea class="aube__input" id="contact-message" name="message" required></textarea>

		<input type="hidden" name="post_id" value="<?php the_ID(); ?>">

		<button class="aube__button" id="contact-submit" type="submit"><?php _e( 'Send ', 'aube' ); ?></button>
	</form>

	<p id="contact-result" aria-hidden="true"></p>
</section>