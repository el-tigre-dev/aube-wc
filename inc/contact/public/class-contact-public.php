<?php
if( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'Aube_WC_Contact_Public' ) ) :

class Aube_WC_Contact_Public {

	public function __construct() {
		add_action( 'wp_ajax_nopriv_aube_contact', array( $this, 'contact' ) );
		add_action( 'wp_ajax_aube_contact', array( $this, 'contact' ) );
	}

	private function get_subject_recipient( $selected_subject, $post_id ) {
		$flexible_sections = get_field( 'section', $post_id );

		foreach ( $flexible_sections as $section ) {
			if ( $section['acf_fc_layout'] === 'section_contact_form' ) {
				foreach ( $section['subjects'] as $subject ) {
					if ( !empty( $subject['recipient'] ) && $selected_subject === $subject['subject'] )
						return $subject['recipient'];
				}

				return $section['recipient'];
			}
		}

		wp_send_json_error( __( 'There is no recipient.', 'aube' ) );
	}

	public function contact() {
		check_ajax_referer( 'aube-contact', 'nonce' );

		if (
			aube_is_filled( $_POST['subject'] ) &&
			aube_is_filled( $_POST['first_name'] ) &&
			aube_is_filled( $_POST['last_name'] ) &&
			aube_is_filled( $_POST['email'] ) &&
			aube_is_filled( $_POST['phone'] ) &&
			aube_is_filled( $_POST['order'] ) &&
			aube_is_filled( $_POST['message'] ) &&
			aube_is_filled( $_POST['post_id'] )
		) {
			if ( !is_email( $_POST['email'] ) )
				wp_send_json_error( __( 'Your email format is incorrect', 'aube' ) );

			$to = $this->get_subject_recipient( $_POST['subject'], $_POST['post_id'] );
			$subject = $_POST['subject'];

			$message = __( 'Contact: ', 'aube' ) . $_POST['first_name'] . ' ' . $_POST['last_name'] . '<br>';
			$message .= __( 'Phone: ', 'aube' ) . $_POST['phone'] . '<br>';
			$message .= __( 'Order: ', 'aube' ) . $_POST['order'] . '<br><br>';
			$message .= __( 'Message: ', 'aube' ) . $_POST['message'] . '<br>';
			$message = nl2br( $message );

			$headers[] = 'Content-type: text/html; charset=UTF-8';
			$headers[] = 'From: ' . $_POST['first_name'] . ' ' . $_POST['last_name'] . ' <' . $_POST['email'] . '>';

			$sent = wp_mail( $to, $subject, $message, $headers );

			if ( !$sent )
				wp_send_json_error( __( 'Your message can not be sent, please retry', 'aube' ) );

			wp_send_json_success( __( 'Your message has been sent, thank you !', 'aube' ) );
		} else {
			wp_send_json_error( __( 'Some fields are required', 'aube' ) );
		}
	}
}

endif;