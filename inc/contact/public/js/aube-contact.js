'use strict';

(function (window, document) {
	var contactForm = document.getElementById('contact-form');
	var contactSubmit = document.getElementById('contact-submit');
	var contactResult = document.getElementById('contact-result');

	function contactResponse(response) {
		contactResult.textContent = response.data;
		contactResult.setAttribute('aria-hidden', false);
		if (response.success) contactSubmit.setAttribute('aria-hidden', true);
	}

	function contact(e) {
		e.preventDefault();

		var args = {
			'form': contactForm,
			'action': aubeContact.actions.contact,
			'nonce': aubeContact.nonce
		};

		post(args, contactResponse, true);
	}

	contactForm.addEventListener('submit', contact);
})(window, document);

//# sourceMappingURL=aube-contact.js.map