<?php
if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'Aube_WC_Contact' ) ) :

class Aube_WC_Contact {

	public function __construct() {
		include_once 'public/class-contact-public.php';
		new Aube_WC_Contact_Public();
	}
}

new Aube_WC_Contact();

endif;