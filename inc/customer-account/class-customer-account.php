<?php
if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'Aube_WC_Customer_Account' ) ) :

class Aube_WC_Customer_Account {

	public function __construct() {
		include_once 'public/class-customer-account-public.php';
	}
}

new Aube_WC_Customer_Account();

endif;