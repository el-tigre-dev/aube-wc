<?php
if( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'Aube_WC_Customer_Account_Public' ) ) :

class Aube_WC_Customer_Account_Public {

	private $dir_path;

	public function __construct() {
		$this->dir_path = get_template_directory() . '/inc/customer-account/public/';
		$this->url_dir_path = get_template_directory_uri() . '/inc/customer-account/public/';

		if ( !is_admin() )
			add_filter( 'woocommerce_account_menu_items', array( $this, 'test_download_item' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'woocommerce_before_account_orders', array( $this, 'orders_search_input' ) );
		add_action( 'woocommerce_account_dashboard', array( $this, 'extended_dashboard' ) );
	}

	/**
	 * Unset downloads link from navigation if customer has no downloadable products
	 */
	public function test_download_item( $items ) {
		$downloads = wc_get_customer_available_downloads( get_current_user_id() );

		if ( empty( $downloads ) )
			unset( $items['downloads'] );

		return $items;
	}

	/**
	 * Add a search input for account orders
	 */
	public function orders_search_input( $has_orders ) {
		if ( $has_orders )
			include 'views/search-input.php';
	}

	public function extended_dashboard() {
		$current_user = wp_get_current_user();
		$customer_id = $current_user->ID;
		$customer_email = esc_html( $current_user->user_email );

		$address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
			'first_name'  => get_user_meta( $customer_id, 'shipping_first_name', true ),
			'last_name'   => get_user_meta( $customer_id, 'shipping_last_name', true ),
			'company'     => get_user_meta( $customer_id, 'shipping_company', true ),
			'address_1'   => get_user_meta( $customer_id, 'shipping_address_1', true ),
			'address_2'   => get_user_meta( $customer_id, 'shipping_address_2', true ),
			'city'        => get_user_meta( $customer_id, 'shipping_city', true ),
			'state'       => get_user_meta( $customer_id, 'shipping_state', true ),
			'postcode'    => get_user_meta( $customer_id, 'shipping_postcode', true ),
			'country'     => get_user_meta( $customer_id, 'shipping_country', true ),
		), $customer_id, 'shipping' );
		$formatted_address = WC()->countries->get_formatted_address( $address );

		$customer_orders = get_posts( 
			array(
				'numberposts' => 3,
				'meta_key'    => '_customer_user',
				'meta_value'  => $current_user->ID,
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() ),
			)
		);

		include_once 'views/extended-dashboard.php';
	}

	public function scripts() {
		if ( !is_wc_endpoint_url( 'orders' ) ) return;

		wp_enqueue_script( 'aube-orders', $this->url_dir_path . 'js/aube-orders.js', array(), Aube::$version, true );
		wp_localize_script( 'aube-orders', 'aubeOrders',
			array(
				'messages' => array( 
					'noOrders' => __( 'No order with this number was found.', 'aube' )
				)
			)
		);
	}
}

new Aube_WC_Customer_Account_Public();

endif;