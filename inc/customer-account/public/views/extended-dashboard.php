<section class="dashboard__boxes">
	<div class="dashboard__box dashboard__info-box">
		<h2 class="info-box__title box__title"><?php _e( 'My information', 'aube' ); ?></h2>

		<div class="info-box__email">
			<p><?php _e( 'Email', 'aube' ); ?></p>
			<p><?php echo $customer_email; ?></p>
		</div>
		<div class="info-box__password">
			<p><?php _e( 'Password', 'aube' ); ?></p>
			<p>*******</p>
		</div>
		<div class="info-box__button box__button">
			<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account' ) ); ?>"><?php _e( 'Modifiy', 'aube' ); ?></a>
		</div>
	</div>

	<div class="dashboard__box dashboard__address-box">
		<h2 class="address-box__title box__title"><?php _e( 'My adresses', 'aube' ); ?></h2>

		<p class="address__title"><?php _e( 'Default shipping address :', 'aube' ); ?></p>
		<?php if ( $formatted_address ) : ?>
			<address class="address__information"><?php echo $formatted_address; ?></address>
		<?php else : ?>
			<p><?php _e( 'No default address set', 'aube'); ?></p>
		<?php endif; ?>
		<div class="address-box__button box__button">
			<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address' ) ); ?>"><?php _e( 'Modify', 'aube' ); ?></a>
		</div>
	</div>

	<div class="dashboard__box dashboard__orders-box">
		<h2 class="account__title box__title"><?php _e( 'My orders', 'aube' ); ?></h2>

		<?php if ( !empty( $customer_orders ) ) : ?>
			<ul class="orders">
				<?php foreach ( $customer_orders as $i => $customer_order ) : 
					$order = wc_get_order( $customer_order );
				?>
					<li>
						<span class="order__number">
							<?php echo $order->get_order_number(); ?>
						</span>
						<span class="order__date">
							<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>">
								<?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?>
							</time>
						</span>
						<span class="order__status">
							<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>
						</span>
					</li>

					<?php if ( $i !== 0 ) : ?>
						<hr class="order__separator" >
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>

			<div class="orders-box__button box__button">
				<a href="<?php echo esc_url( wc_get_endpoint_url( 'orders' ) ); ?>"><?php _e( 'See more', 'aube' ); ?></a>
			</div>
		<?php else : ?>
			<p><?php _e( 'No orders yet', 'aube'); ?></p>
		<?php endif; ?>
	</div>
</section>