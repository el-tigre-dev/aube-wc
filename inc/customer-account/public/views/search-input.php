<input
	id="aube-order-search-input"
	type="number"
	placeholder="<?php _e( 'Order number (ex. 55)', 'aube' ); ?>"
	autocomplete="off"
/>
<button id="aube-order-search-reset"><?php _e( 'Reset', 'aube' ); ?></button>
<p id="aube-order-search-result"></p>