<?php
if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'Aube_WC_Ajax_Sign' ) ) :

class Aube_WC_Ajax_Sign {

	public function __construct() {
		include_once 'public/class-ajax-sign-public.php';
		new Aube_WC_Ajax_Sign_Public();
	}
}

new Aube_WC_Ajax_Sign();

endif;