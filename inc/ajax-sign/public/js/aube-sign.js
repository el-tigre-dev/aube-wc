'use strict';

(function (window, document) {

	/* SIDEBAR */

	var accountLinks = document.querySelectorAll('a[href$="mon-compte/"], a[href$="my-account/"]');
	var signSidebar = document.getElementById('aube-sign-sidebar');
	var signSidebarCloser = document.getElementById('aube-sign-sidebar-closer');

	for (var i = accountLinks.length - 1; i >= 0; i--) {
		accountLinks[i].addEventListener('click', function (e) {
			if (aube.userLogged !== '0') return;

			e.preventDefault();
			if (!signSidebar.classList.contains('aube-sign-sidebar--opened')) signSidebar.classList.add('aube-sign-sidebar--opened');
		});
	}

	signSidebarCloser.addEventListener('click', function (e) {
		signSidebar.classList.remove('aube-sign-sidebar--opened');
	});

	/* SECTIONS DISPLAY */

	var signInButtons = document.getElementsByClassName('aube-sign-in__button');
	var signInSection = document.getElementById('sign-in-section');
	var signUpSection = document.getElementById('sign-up-section');
	var signUpButton = document.getElementById('sign-up-button');
	var retrievePasswordSection = document.getElementById('retrieve-password-section');
	var retrievePasswordButton = document.getElementById('retrieve-password-button');

	for (var i = signInButtons.length - 1; i >= 0; i--) {
		signInButtons[i].addEventListener('click', function (e) {
			signInSection.setAttribute('aria-hidden', false);
			signUpSection.setAttribute('aria-hidden', true);
			retrievePasswordSection.setAttribute('aria-hidden', true);
		});
	}

	signUpButton.addEventListener('click', function (e) {
		signInSection.setAttribute('aria-hidden', true);
		signUpSection.setAttribute('aria-hidden', false);
	});

	retrievePasswordButton.addEventListener('click', function (e) {
		signInSection.setAttribute('aria-hidden', true);
		retrievePasswordSection.setAttribute('aria-hidden', false);
	});

	/* SIGN IN */

	var signInForm = document.getElementById('sign-in-form');
	var signInSubmit = document.getElementById('sign-in-submit');
	var signInResult = document.getElementById('sign-in-result');

	function signInResponse(response) {
		if (response.success) {
			document.location.href = response.data;
		} else {
			signInResult.textContent = response.data;
			signInResult.setAttribute('aria-hidden', false);
		}
		signInSubmit.removeAttribute('disabled');
	}

	function signIn(e) {
		e.preventDefault();
		signInSubmit.setAttribute('disabled', true);

		var args = {
			'form': signInForm,
			'action': aubeSign.actions.signIn,
			'nonce': aubeSign.nonce
		};

		post(args, signInResponse, true);
	}

	signInForm.addEventListener('submit', signIn);

	/* SIGN UP */

	var signUpFormContainer = document.getElementById('sign-up-form-container');
	var signUpForm = document.getElementById('sign-up-form');
	var signUpSubmit = document.getElementById('sign-up-submit');
	var signUpResult = document.getElementById('sign-up-result');

	function signUpResponse(response) {
		if (response.success) {
			document.getElementById('sign-up-thank-container').setAttribute('aria-hidden', false);
			document.getElementById('sign-up-form-container').setAttribute('aria-hidden', true);
		} else {
			signUpResult.textContent = response.data;
			signUpResult.setAttribute('aria-hidden', false);
		}
		signUpSubmit.removeAttribute('disabled');
	}

	function signUp(e) {
		e.preventDefault();
		signUpSubmit.setAttribute('disabled', true);

		var args = {
			'form': signUpForm,
			'action': aubeSign.actions.signUp,
			'nonce': aubeSign.nonce
		};

		post(args, signUpResponse, true);
	}

	signUpForm.addEventListener('submit', signUp);

	/* RETRIEVE PASSWORD */

	var retrievePasswordForm = document.getElementById('retrieve-password-form');
	var retrievePasswordSubmit = document.getElementById('retrieve-password-submit');
	var retrievePasswordResult = document.getElementById('retrieve-password-result');

	function retrievePasswordResponse(response) {
		if (response.success) {
			document.getElementById('retrieve-password-form').setAttribute('aria-hidden', true);
			retrievePasswordResult.textContent = response.data;
			retrievePasswordResult.setAttribute('aria-hidden', false);
		} else {
			retrievePasswordResult.textContent = response.data;
		}
	}

	function retrievePassword(e) {
		e.preventDefault();

		var args = {
			'form': retrievePasswordForm,
			'action': aubeSign.actions.retrievePassword,
			'nonce': aubeSign.nonce
		};

		post(args, retrievePasswordResponse, true);
	}

	retrievePasswordForm.addEventListener('submit', retrievePassword);
})(window, document);

//# sourceMappingURL=aube-sign.js.map