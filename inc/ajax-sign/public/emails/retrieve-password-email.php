<?php
if ( ! defined( 'ABSPATH' ) )
    exit;

echo wc_get_template( 'emails/email-header.php', array( 'email_heading' => $title ) );
?>

<p>
    <?php _e( 'A password reset has been asked for your account.', 'aube' ); ?><br>
    <?php _e( 'If this is an error, just ignore this email.', 'aube' ); ?><br>
    <?php _e( 'To make a new password, follow this link:', 'aube' ); ?><br><br>
    <a href="<?php echo $lost_password_url . '?key=' . $key . '&login=' . rawurlencode( $user->user_login ); ?>">
        <?php echo $lost_password_url . '?key=' . $key . '&login=' . rawurlencode( $user->user_login ); ?>
    </a>
</p>
<p>
    <?php _e( 'See you soon,', 'aube' ); ?><br>
</p>

<?php echo wc_get_template( 'emails/email-footer.php' ); ?>