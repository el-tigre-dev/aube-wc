<?php
/* TODO
	- Appeler bon email
*/

if( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'Aube_WC_Ajax_Sign_Public' ) ) :

class Aube_WC_Ajax_Sign_Public {

	private $dir_path;

	public function __construct() {
		$this->dir_path = get_template_directory_uri() . '/inc/ajax-sign/public/';

		add_action( 'wp_ajax_nopriv_aube_sign_in', array( $this, 'sign_in' ) );
		add_action( 'wp_ajax_aube_sign_in', array( $this, 'sign_in' ) );
		add_action( 'wp_ajax_nopriv_aube_sign_up', array( $this, 'sign_up' ) );
		add_action( 'wp_ajax_aube_sign_up', array( $this, 'sign_up' ) );
		add_action( 'wp_ajax_nopriv_aube_retrieve_password', array( $this, 'retrieve_password' ) );
		add_action( 'wp_ajax_aube_retrieve_password', array( $this, 'retrieve_password' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );

		add_shortcode( 'aube-sign-sidebar', array( $this, 'sign_sidebar' ) );
	}

	public function scripts() {
		wp_enqueue_script( 'aube-sign', $this->dir_path . 'js/aube-sign.js', array( 'aube' ), Aube::$version, true );
		wp_localize_script( 'aube-sign', 'aubeSign',
			array(
				'nonce'   => wp_create_nonce( 'aube-sign' ),
				'actions' => array(
					'signIn' => 'aube_sign_in',
					'signUp' => 'aube_sign_up',
					'retrievePassword' => 'aube_retrieve_password'
				),
				'messages' => array(
					'error' => array(
						'server' => __( 'Server error, please retry', 'aube' ),
						'connection' => __( 'Connection problem, please retry', 'aube' ),
					)
				)
			)
		);
	}

	public function sign_sidebar() {
		include_once 'views/sign-sidebar.php';
	}

	public function sign_in() {
		check_ajax_referer( 'aube-sign', 'nonce' );

		if ( is_user_logged_in() )
			wp_send_json_error( __( 'You are already logged in', 'aube' ) );
		
		if (
			aube_is_filled( $_POST['email'] ) &&
			aube_is_filled( $_POST['password'] ) 
		) {
			if ( !is_email( $_POST['email'] ) )
				wp_send_json_error( __( 'Your email format is incorrect', 'aube' ) );

			$user = wp_signon( array(
				'user_login'    => $_POST['email'],
				'user_password' => $_POST['password'],
				'remember'      => true
			) );

			if ( is_wp_error( $user ) )
				wp_send_json_error( __( 'Your credentials seem incorrect', 'aube' ) );

			wp_set_current_user( $user->ID, $user->user_login );

			wp_send_json_success( wc_get_page_permalink( 'myaccount' ) );
		} else {
			wp_send_json_error( __( 'All fields are required', 'aube' ) );
		}
	}

	public function sign_up() {
		check_ajax_referer( 'aube-sign', 'nonce' );

		if ( 
			aube_is_filled( $_POST['first_name'] ) && 
			aube_is_filled( $_POST['last_name'] ) &&
			aube_is_filled( $_POST['email'] ) &&
			aube_is_filled( $_POST['password'] ) && 
			aube_is_filled( $_POST['password_confirmation'] ) &&
			aube_is_filled( $_POST['gsc'] )
		) {
			if ( !is_email( $_POST['email'] ) )
				wp_send_json_error( __( 'Your email format is incorrect', 'aube' ) );

			if ( email_exists( $_POST['email'] ) )
				wp_send_json_error( __( 'This email is already used', 'aube' ) );

			if ( $_POST['password'] !== $_POST['password_confirmation'] )
				wp_send_json_error( __( 'Your passwords are not the same', 'aube' ) );

			$user_id = wp_create_user( $_POST['email'], $_POST['password'], $_POST['email'] );
			$user = wp_signon( array(
				'user_login'    => $_POST['email'],
				'user_password' => $_POST['password'],
				'remember'      => true
			) );

			if ( is_wp_error( $user_id ) || is_wp_error( $user ) )
				wp_send_json_error( __( 'An error has occured, please retry', 'aube' ) );

			$first_name = sanitize_text_field( $_POST['first_name'] );
			$last_name = sanitize_text_field( $_POST['last_name'] );

			wp_update_user( array(
				'ID'         => $user->ID,
				'first_name' => $first_name,
				'last_name'  => $last_name,
			) );
			update_user_meta( $user->ID, 'billing_first_name', $first_name );
			update_user_meta( $user->ID, 'billing_last_name', $last_name );

			wp_set_current_user( $user->ID, $user->user_login );

			wp_send_json_success();
		} else {
			wp_send_json_error( __( 'All fields are required', 'aube' ) );
		}
	}

	public function retrieve_password() {
		check_ajax_referer( 'aube-sign', 'nonce' );

		if ( !aube_is_filled( $_POST['email'] ) )
			wp_send_json_error( __( 'Email address is missing', 'aube' ) );

		if ( !is_email( $_POST['email'] ) )
			wp_send_json_error( __( 'Email address is incorrect', 'aube' ) );

		$user = get_user_by( 'email', trim( wp_unslash( $_POST['email'] ) ) );

		if ( empty( $user ) )
			wp_send_json_error( __( 'There is not user with this email', 'aube' ) );

		$key = get_password_reset_key( $user );

		if ( is_wp_error( $key ) )
			wp_send_json_error( __( 'An error has occured, please retry', 'aube' ) );

		$lost_password_url = wp_lostpassword_url();

		$title = __( 'Password reset', 'aube' );

		ob_start();
		include 'emails/retrieve-password-email.php';
		$message = ob_get_clean();

		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = 'From: XXX <contact@el-tigre.net>';

		if ( !wp_mail( $_POST['email'], $title, $message, $headers ) )
			wp_send_json_error( __( 'We are not able to send the email', 'aube' ) );

		wp_send_json_success( __( 'An email has been sent with your reset link', 'aube' ) );
	}
}

endif;