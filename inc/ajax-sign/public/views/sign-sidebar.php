<aside id="aube-sign-sidebar" class="aube-sign-sidebar">
	<button id="aube-sign-sidebar-closer" class="aube-sign-sidebar__closer">X</button>

	<div class="aube-sign-sidebar__content">
		<?php
		include_once 'sign-in-form.php';
		include_once 'sign-up-form.php';
		include_once 'retrieve-password-form.php';
		?>
	</div>
</aside>