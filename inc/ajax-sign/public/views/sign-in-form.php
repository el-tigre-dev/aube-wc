<section id="sign-in-section" class="aube-sign-in__section aube-sign-in__section--opened" aria-hidden="false">
	<p><?php _e( 'Sign in', 'aube' ); ?></p>

	<form id="sign-in-form" novalidate>
		<label class="aube__label" for="sign-in-email"><?php _e( 'Email', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" type="email" name="email" id="sign-in-email" placeholder="<?php _e( 'e.g. john@doe.com', 'aube' ); ?>" required>

		<label class="aube__label" for="sign-in-password"><?php _e( 'Password', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" type="password" name="password" id="sign-in-password" placeholder="<?php _e( 'e.g. ••••••••••••', 'aube' ); ?>" required>

		<button class="aube__button" id="sign-in-submit" type="submit"><?php _e( 'Connection', 'aube' ); ?></button>
	</form>

	<p id="sign-in-result" aria-hidden="true"></p>

	<button class="aube__button" id="retrieve-password-button"><?php _e( 'Lost your password?', 'aube' ); ?></button>
	<button class="aube__button" id="sign-up-button"><?php _e( 'Sign up', 'aube' ); ?></button>
</section>