<section id="retrieve-password-section" class="aube-retrieve-password__section" aria-hidden="true">
	<p><?php _e( 'Forgotten password', 'aube' ); ?></p2>

	<form id="retrieve-password-form" novalidate>
		<p><?php _e( 'Type your email to receive a reset password link', 'aube' ); ?></p>

		<label class="aube__label" for="retrieve-password-email"><?php _e( 'Email', 'aube' ); ?><span class="required">*</span></label>
		<input class="aube__input" id="retrieve-password-email" type="email" name="email" placeholder="<?php _e( 'e.g. john@doe.com', 'aube' ); ?>" required>

		<button class="aube__button" id="retrieve-password-submit" type="submit"><?php _e( 'Receive the email', 'aube' ); ?></button>
	</form>

	<p id="retrieve-password-result" aria-hidden="true"></p>

	<button class="aube__button aube-sign-in__button"><?php _e( 'Back', 'aube' ); ?></button>
</section>