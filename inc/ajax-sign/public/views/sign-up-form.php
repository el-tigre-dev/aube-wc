<section id="sign-up-section" class="aube-sign-up__section" aria-hidden="true">
	<p><?php _e( 'Sign up', 'aube' ); ?></p>

	<div id="sign-up-form-container">
		<form id="sign-up-form" novalidate autocomplete="off">
			<label class="aube__label" for="sign-up-first-name"><?php _e( 'First name', 'aube' ); ?><span class="required">*</span></label>
			<input class="aube__input" type="text" name="first_name" id="sign-up-first-name" placeholder="<?php _e( 'e.g. John', 'aube' ); ?>" required>

			<label class="aube__label" for="sign-up-last-name"><?php _e( 'Last name', 'aube' ); ?><span class="required">*</span></label>
			<input class="aube__input" type="text" name="last_name" id="sign-up-last-name" placeholder="<?php _e( 'e.g. Doe', 'aube' ); ?>"  required>

			<label class="aube__label" for="sign-up-email"><?php _e( 'Email', 'aube' ); ?><span class="required">*</span></label>
			<input class="aube__input" type="email" name="email" id="sign-up-email" placeholder="<?php _e( 'e.g. john@doe.com', 'aube' ); ?>"  required>

			<label class="aube__label" for="sign-up-password"><?php _e( 'Password', 'aube' ); ?><span class="required">*</span></label>
			<input class="aube__input" type="password" name="password" id="sign-up-password" placeholder="<?php _e( 'e.g. ••••••••••••', 'aube' ); ?>" required>

			<label class="aube__label" for="sign-up-password-confirmation"><?php _e( 'Confirm your password', 'aube' ); ?><span class="required">*</span></label>
			<input class="aube__input" type="password" name="password_confirmation" id="sign-up-password-confirmation" placeholder="<?php _e( 'e.g. •••••••••••• again', 'aube' ); ?>" data-confirm="sign-up-password" required>

			<div>
				<input type="checkbox" id="sign-up-gsc" name="gsc" required>
				<label class="aube__label" for="sign-up-gsc">
					<?php printf( __( 'I accept the %s', 'aube' ), '<a href="">' . __( 'General Selling Conditions', 'aube' ) . '</a>' ); ?>
				</label>
			</div>

			<button class="aube__button" id="sign-up-submit" type="submit"><?php _e( 'Sign up', 'aube' ); ?></button>
		</form>

		<p id="sign-up-result" aria-hidden="true"></p>

		<button class="aube__button aube-sign-in__button"><?php _e( 'Sign in', 'aube' ); ?></button>
	</div>

	<div id="sign-up-thank-container" class="aube-sign-up__thank-container" aria-hidden="true">
		<p><?php _e( 'Thanks for creating your account !', 'aube' ); ?></p>
		<p><?php _e( 'You will receive a confirmation email', 'aube' ); ?></p>
		<a href="<?php wc_get_page_permalink( 'myaccount' ); ?>"><?php _e( 'My account', 'aube' ); ?></a>
		<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>"><?php _e( 'Go shopping', 'aube' ); ?></a>
	</div>
</section>