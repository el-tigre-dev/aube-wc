<?php
class Aube_Maintenance {

	public function __construct() {       
        $this->aube_activate_maintenance();
    }
    
    function aube_activate_maintenance() {
		$if_maintenance_mode = get_field( 'maintenance_mode', 'global-options' );
		
    	if ( $if_maintenance_mode )
    		add_action('get_header', array ( $this ,'activate_maintenance_mode' ) );
    }

	function activate_maintenance_mode() {
		if ( !( current_user_can( 'administrator' ) ||  current_user_can( 'super admin' ) ) ) {
			$url_redirect = get_field( 'url_redirect', 'global-options' );
			wp_redirect( $url_redirect, 302 ); exit;
		}
	}

}
new Aube_Maintenance();
?>