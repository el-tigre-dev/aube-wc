<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aube
 */
?>

	</main><!-- .site__content -->
	<div class="pre-footer" style="background-image:url(<?php echo get_field( 'background_image', 'global-options' )['url'] ;?>)">
		<?php
			$blocks = get_field( 'reassurance_block', 'global-options' );	
			$rs = get_field( 'reseaux_sociaux', 'global-options' );
		?>
		<?php if( !empty( $blocks ) ) : foreach ($blocks as $block) : ?>
			<div class="pre-footer__section" >
				<a href="<?php echo $block['link']; ?>"><div><img class="pre-footer__picto"  src="<?php echo $block['picto']['url']; ?>" alt="pictogramme" ></div>
					<h3><?php echo $block['title']; ?></h3>
					<p><?php echo $block['description']; ?></p>
				</a>
			</div>
		<?php endforeach; endif; ?>	
	</div>
	<footer class="site__footer" role="contentinfo">
		<div class="footer__top">
			<div class="col">
				<span class="footer__col-title"><?php _e( 'NEWSLETTER', 'aube'); ?></span>
				<form class="footer__form" method="GET" onsubmit="return false">
					<input type="email" placeholder="<?php _e( 'Votre email', 'aube'); ?>" id="newsletter-input" class="newsletter-input">
					<input type="submit" value="<?php _e( 'S\'inscrire', 'aube'); ?>" id="newsletter-submit" class="newsletter-submit">						
				</form>
				<div id="news-result" class="news-result"></div>
			</div>
			<?php
			if ( has_nav_menu( 'site-footer' ) ) {
				include_once 'inc/walkers/class-site-footer-menu-walker.php';
				wp_nav_menu( array( 
					'theme_location'  => 'site-footer',
					'menu_id'         => 'site-footer-menu',
					'menu_class'      => 'site__footer-menu',
					'container_class' => 'site__footer-menu-container',
					'walker'          => new Site_Footer_Menu_Walker()
				) );
			}
			?>	
		</div>
		<?php if( !empty( $rs ) ) : ?>
			<div class="footer__bottom">
				<div class="bottom-buttons">
					<p><?php _e( 'Suivez nous', 'aube-woocommerce' ); ?></p>
					<?php foreach ($rs as $socials) : ?>
						<?php if( !empty($socials['link'] ) ) : ?>
							<a class="container-social" href="<?php echo $socials['link']; ?>" target="_blank" >
								<img src="<?php echo $socials['social_img']['url']; ?>" >
							</a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>	
		<?php endif; ?>
	</footer><!-- .site__footer -->
</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>