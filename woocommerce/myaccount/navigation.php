<?php
/**
 * My Account navigation
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$current_url = aube_current_url();

do_action( 'woocommerce_before_account_navigation' );
?>

<nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : 
			$endpoint_url = esc_url( wc_get_account_endpoint_url( $endpoint ) );
			$current = $endpoint_url === $current_url ? 'page' : 'false';
		?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a aria-current="<?php echo $current; ?>" href="<?php echo $endpoint_url; ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>