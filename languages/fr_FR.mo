��    b      ,      <      <     =  5   @  1   v     �  +   �  "   �                %     8     K     `  
   v      �  	   �     �     �     �     �     �  
     	         *     6     I     d     p     |  ,   �  E   �  W   �  	   W	     a	     j	     �	     �	     �	  	   �	     �	     �	  
   �	     �	     �	  	   �	     �	     �	  $   
     5
     C
     R
     `
  %   o
     �
     �
     �
     �
     �
     �
     �
     �
     �
                    2     ;     I     O     j     r     z     �  "   �     �  !   �     �  )     0   @  !   q     �  %   �     �     �  *     '   =     e  T   �  
   �     �  	   �     �  )   
  /   4     d     {     �     �  �  �     I  0   K  J   |     �  =   �  ,   !     N     _     f     y     �     �  	   �  +   �  	          "   *     M     S     r     �     �     �     �     �     �     �       3     T   S  X   �               	     '     >     C  	   K     U     ^  
   g     r          �     �     �  3   �               +     B  )   W     �     �  
   �     �  !   �     �                    *     ;     M  	   b     l     y  &   �     �     �     �     �  #   �       )   1  (   [  4   �  =   �  &   �       ,   :  $   g  *   �  8   �  &   �  (        @  	   [     e     m     v  (   �  3   �     �     �             ,  A WordPress Starter theme for WooCommerce with Brunch A password reset has been asked for your account. All fields are required An email has been sent with your reset link An error has occured, please retry Aube WooCommerce Back Clément Leboucher Comment navigation Comments are closed. Confirm your password Connection Connection problem, please retry Contact:  Continue reading %s Default shipping address : Email Email address is incorrect Email address is missing First name Firstname Footer Menu Forgotten password General Selling Conditions Go shopping Header Menu I accept the %s If this is an error, just ignore this email. It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Last name Lastname Leave a Comment on %s Lost your password? Menu Message Message:  Modifiy Modify My account My adresses My information My orders Newer Comments No default address set No order with this number was found. No orders yet No subject yet Nothing Found Older Comments Oops! That page can&rsquo;t be found. Order number Order number (ex. 55) Order:  Password Password reset Phone number Phone:  Pop-in Posted in %1$s Receive the email Reset Search Results for: %s See more See you soon, Send  Server error, please retry Sign in Sign up Some fields are required Tagged %1$s Thanks for creating your account ! There is no recipient. There is not user with this email This email is already used To make a new password, follow this link: Type your email to receive a reset password link We are not able to send the email You are already logged in You will receive a confirmation email Your credentials seem incorrect Your email format is incorrect Your message can not be sent, please retry Your message has been sent, thank you ! Your passwords are not the same comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; e.g. 11305 e.g. Doe e.g. John e.g. john@doe.com e.g. •••••••••••• e.g. •••••••••••• again http://underscores.me/ https://el-tigre.net post authorby %s post datePosted on %s Project-Id-Version: Aube WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-06-05 13:37+0000
PO-Revision-Date: 2018-06-05 13:38+0000
Last-Translator: clement <clement@el-tigre.net>
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ , Un thème WordPress pour WooCommerce avec Brunch Une réinitialisation de mot de passe a été demandée pour votre compte. Tous les champs sont requis Un email a été envoyé avec votre lien de réinitialisation Une erreur est survenue, merci de réessayer Aube WooCommerce Retour Clément Leboucher Navigation du commentaire Les commentaires sont fermés. Confirmez votre mot de passe Connexion Problème de connexion, merci de réessayer Contact : Continuer la lecture de %s Adresse de livraison par défaut : Email L'adresse email est incorrecte L'adresse email est requise Prénom Prénom Menu pied de page Mot de passe oublié Conditions Générales de Vente Faire mon shopping Menu en-tête de page J'accepte les %s Si c'est une erreur, vous pouvez ignorer cet email. On dirait que rien n'a été trouvé. Peut-être devriez-vous essayer la recherche ? On dirait que rien n'a été trouvé. Peut-être que vous devriez essayer la recherche ? Nom Nom Laisser un commentaire sur %s Mot de passe oublié ? Menu Message Message : Modifier Modifier Mon compte Mes adresses Mes informations Mes commandes Nouveaux commentaires Pas d'adresse par défaut Aucune commande avec ce numéro n'a été trouvée. Pas de commandes Pas de sujet disponible Rien n'a été trouvé Anciens commentaires Oops ! Cette page ne peut être trouvée. Numéro de commande Numéro de commande (ex. 55) Commande : Mot de passe Réinitialisation de mot de passe Numéro de téléphone Téléphone : Pop-in Publié dans %1$s Recevoir l'email Réinitialisation Résultats pour : %s Voir plus À bientôt, Envoyer Erreur du serveur, merci de réessayer Se connecter S'enregistrer Des champs sont requis Étiqueté avec %1$s Merci d'avoir créé votre compte ! Il n'y a pas de destinataire. Il n'y a pas d'utilisateur avec cet email Cette adresse email est déjà utilisée Pour faire un nouveau mot de passe, suivez ce lien : Entrez votre email pour recevoir un lien de réinitialisation Nous n'arrivons pas à envoyer l'email Vous êtes déjà connecté Vous allez recevoir un email de confirmation Vos identifiants semblent incorrects Votre format d'adresse email est incorrect Votre message ne peut être envoyer, merci de réessayer Votre message a été envoyé, merci ! Vos mots de passe ne sont pas identiques Les commentaires sur %2$s  ex. 11305 ex. Doe ex. John ex. john@doe.com ex. •••••••••••• ex. •••••••••••• de nouveau http://underscores.me/ https://el-tigre.net par %s Publié le %s 