module.exports = {
	files: {
		javascripts: {
			// To join all files from a dir to a single file (use for page linked scripts)
			// 'js/home.js' : '/^app\/home/'
			joinTo: {
				'js/aube.js': 'dev/js/aube.js',
				'js/shop-page.js': 'dev/js/shop-page.js',
				'js/aube-color-picker.js': 'dev/js/aube-color-picker.js',
				'js/aube-banner-swiper.js': 'dev/js/aube-banner-swiper.js',
				'js/aube-faq.js': 'dev/js/aube-faq.js',
				'js/aube-new-products.js': 'dev/js/aube-new-products.js',
				'inc/customer-account/public/js/aube-orders.js': 'dev/js/customer-account/aube-orders.js',
				'inc/ajax-sign/public/js/aube-sign.js': 'dev/js/ajax-sign/aube-sign.js',
				'inc/contact/public/js/aube-contact.js': 'dev/js/contact/aube-contact.js',
				'inc/pop-in/public/js/aube-pop-in.js': 'dev/js/pop-in/aube-pop-in.js',
				'inc/banner/public/js/aube-banner.js': 'dev/js/banner/aube-banner.js',
			}
		},
		stylesheets: { joinTo: 'styles/style.css' }
	},
	paths: {
		public: '',
		watched: ['dev']
	},
	plugins: {
		browserSync: {
			files: ["*.php"],
			snippetOptions: {
				rule: {
					match: /<\head>/i,
					fn: function (snippet, match) {
						return snippet + match;
					}
				}
			}
		}
	},
	// Disable CommonJS modules
	npm: {
		enabled: false
	},
	modules: {
		wrapper: false,
		definition: false
	}
}