<?php 
// Template Name: Page Flexible

get_header();
$fields = get_fields();
?>

<div class="flexible-sections">
	<?php include( locate_template( 'template-parts/flexible/flexible-sections.php' ) ); ?>
</div>

<?php get_footer(); ?>