<?php
class Aube {

	public static $version;

	function __construct() {
		$theme = wp_get_theme();
		self::$version = $theme['Version'];

		add_filter( 'stylesheet_uri', array( $this, 'change_stylesheet_uri' ), 10, 2 );
		add_filter( 'wp_revisions_to_keep', array( $this, 'posts_revisions_number' ), 10, 2 );

		add_filter( 'tiny_mce_before_init', array( $this,'aube_color_options' ), 10, 2 );
		add_action( 'acf/input/admin_enqueue_scripts', array( $this, 'admin_scripts') );

		add_action( 'after_setup_theme', array( $this, 'add_theme_supports' ) );
		add_action( 'after_setup_theme', array( $this, 'register_menus' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'register_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );

		// WooCommerce configuration
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
		add_action('woocommerce_before_main_content', array( $this, 'aube_wc_before_main_content' ), 10 );
		add_action('woocommerce_after_main_content', array( $this, 'aube_wc_after_main_content' ), 10 );
		add_action( 'login_enqueue_scripts', array( $this, 'aube_login_logo' ) );

		$this->load_extras();
		$this->acf_options();
		$this->load_dependencies();
	}

	function add_theme_supports() {
		load_theme_textdomain( 'aube', get_template_directory() . '/languages' );

		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'custom-logo' );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// WooCommerce support
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

	function register_menus() {
		register_nav_menus( array(
			'site-header' => esc_html__( 'Header Menu', 'aube' ),
		));

		register_nav_menus( array(
			'site-footer' => esc_html__( 'Footer Menu', 'aube' ),
		));
	}

	function change_stylesheet_uri( $stylesheet_uri, $stylesheet_dir_uri ) {
		return $stylesheet_dir_uri . '/styles';
	}

	private function get_scripts_uri() {
		return get_template_directory_uri() . '/js';
	}

	function register_styles() {
		wp_enqueue_style( 'aube', get_stylesheet_uri() . '/style.css' );

		// Vendor
		wp_enqueue_style( 'swiper', get_stylesheet_uri() . '/vendor/swiper.min.css' );
		wp_enqueue_style( 'object-fit', get_stylesheet_uri() . '/vendor/object-fit.min.css' );
	}

	function register_scripts() {
		// Vendor		
		wp_register_script( 'swiper' , $this->get_scripts_uri() . '/vendor/swiper.min.js' , array() , false , true );			
		wp_register_script( 'infinite-scroll', $this->get_scripts_uri() . '/vendor/infinite-scroll.min.js', array(), self::$version, true );
		
		wp_register_script( 'aube', $this->get_scripts_uri() . '/aube.js', array(), self::$version, true );
		wp_localize_script( 'aube', 'aube', 
			array( 
				'ajaxUrl' => admin_url( 'admin-ajax.php' ),
				'userLogged' => is_user_logged_in() === true ? 1 : 0,
				'messages' => array(
					'error' => array(
						'server' => __( 'Server error, please retry', 'aube' ),
						'connection' => __( 'Connection problem, please retry', 'aube' ),
					)
				)
			)
		);

		// Flexible
		wp_register_script( 'aube-faq', $this->get_scripts_uri() . '/aube-faq.js', array(), false, true );
		wp_register_script( 'aube-banner-swiper', $this->get_scripts_uri() . '/aube-banner-swiper.js' , array( 'swiper' , 'jquery' ), false , true );
		wp_register_script( 'aube-new-products', $this->get_scripts_uri() . '/aube-new-products.js', array( 'swiper' , 'jquery' ), false, true );		
		
		
		// IAS for Products Pages
		wp_register_script( 'shop-page', $this->get_scripts_uri() . '/shop-page.js', array(), self::$version, true );						
		if ( is_shop() || is_product_category() ){
			wp_enqueue_script( 'infinite-scroll' );
			wp_enqueue_script( 'shop-page' );
		}	

		//wp_deregister_script( 'jquery' );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	private function load_extras() {
		include_once 'inc/helper-functions.php';
		include_once 'inc/template-tags.php';
	}

	private function load_dependencies() {
		if ( is_user_logged_in() )
			include_once 'inc/customer-account/class-customer-account.php';

		include_once 'inc/ajax-sign/class-ajax-sign.php';
		include_once 'inc/contact/class-contact.php';
		include_once 'inc/pop-in/class-pop-in.php';
		include_once 'inc/maintenance.php';
	}

	function aube_wc_before_main_content() { 
		?>
			<section id="main">
		<?php
	}

	function aube_wc_after_main_content() { 
		?>
			</section>
		<?php
	}

	function aube_login_logo() {
	?>
		<style type="text/css">
			#login h1 a,
			.login h1 a {
				background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/styles/logo-el-tigre.png);
			}
		</style>
	<?php
	}

	function posts_revisions_number( $num ) {
		return 3;
	}

	private function acf_options() {
		if ( !function_exists( 'acf_add_options_page' ) ) return;


		$parent = acf_add_options_page( array(
			'page_title' => __( 'Global Options', 'aube' ),
			'menu_title' => __( 'Global Options', 'aube' ),
			'menu_slug'  => 'global-options',
			'post_id'    => 'global-options'
		) );
	}

	function aube_color_options( $init ) {
		$custom_colours = '
			"00966C", "Color 1",
			"F8C1B8", "Color 2",
			"FFDCD5", "Color 3",
			"C0A571", "Color 4",
			"ffffff", "White",
		';
		$init['textcolor_map'] = '['.$custom_colours.']';
		$init['textcolor_rows'] = 4; 
	   
		return $init;
	}

	function admin_scripts($hook) {
		wp_enqueue_script( 'aube-color-picker' , $this->get_scripts_uri() . '/aube-color-picker.js' , array( 'jquery','acf-input' ), false, true );  
	}


}

new Aube();