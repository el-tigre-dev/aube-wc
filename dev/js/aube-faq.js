'use strict';
/**
 * Created by Dav on 21/06/18.
 */
function toggleItem(id) {
    var question = document.getElementById('question' + id);

    var x = document.getElementById("paragraph" + id);
    if (x.style.display === "block") {
        x.style.display = "none";
        question.classList.add('question__down-arrow');
        question.classList.remove('question__up-arrow');

    } else {
        x.style.display = "block";
        question.classList.remove('question__down-arrow');
        question.classList.add('question__up-arrow');
    }
}
;
//# sourceMappingURL=toggle.js.map