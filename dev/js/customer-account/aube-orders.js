/**
 * Filter orders rows by order number
 */
(function (window, document) {
	var orderSearchInput = document.getElementById('aube-order-search-input');
	var orderSearchReset = document.getElementById('aube-order-search-reset');
	var orderSearchResult = document.getElementById('aube-order-search-result');
	var ordersRows = document.getElementsByClassName('woocommerce-orders-table__cell-order-number');

	function resetOrders() {
		for (var i = ordersRows.length - 1; i >= 0; i--)
			ordersRows[i].parentNode.setAttribute('aria-hidden', false);

		orderSearchResult.textContent = '';
		orderSearchInput.value = '';
	}

	function filterOrders() {
		var ordersDisplayedNumber = 0;

		for (var i = ordersRows.length - 1; i >= 0; i--) {
			var displayValue = ordersRows[i].querySelector('a[href*="' + this.value + '"]') == null && this.value != '';

			if (!displayValue)
				ordersDisplayedNumber++;

			ordersRows[i].parentNode.setAttribute('aria-hidden', displayValue);
		}

		orderSearchResult.textContent = !ordersDisplayedNumber ? aubeOrders.messages.noOrders : '';
	}

	orderSearchReset.addEventListener('click', resetOrders);
	orderSearchInput.addEventListener('input', filterOrders);
})(window, document);