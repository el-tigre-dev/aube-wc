'use strict';
(function () {
	var swipers = document.querySelectorAll('.section-new-products .swiper-container');
	if (swipers && swipers.length > 0) {
		for (var index = 0; index < swipers.length; index++) {
			var swiper = swipers[index];
			swiper.setAttribute('class', 'swiper-container slider-new-products-' + index);
			var swiperClassName = '.slider-new-products-' + index;
			new Swiper(swiperClassName, {
				onInit: function onInit(swiper) {
					if (swiper.slides.length == 1) {
						nextButton = document.querySelector(swiperClassName + ' .swiper-button-next-np');
						prevButton = document.querySelector(swiperClassName + ' .swiper-button-prev-np');
						nextButton.style.display = 'none';
						prevButton.style.display = 'none';
						swiper.params.loop = false;
					} else {
						swiper.params.loop = true;
					}
				},
				// Optional parameters
				direction: 'horizontal',
				slidesPerView: 3,
				initialSlide: 0,
				loop: true,
				spaceBetween: 20,

				breakpoints: {
					1024: {
						slidesPerView: 2
					},
					768: {
						slidesPerView: 1
					},
				},
				// Navigation arrows
				navigation: {
					nextEl: swiperClassName + ' .swiper-button-next-np',
					prevEl: swiperClassName + ' .swiper-button-prev-np',
				},
				scrollbar: {
					el: '.swiper-scrollbar',
				}
			});
		}
	}
})();